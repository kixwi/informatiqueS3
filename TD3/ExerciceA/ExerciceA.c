/*
 * Author : Killian LOUISE TP2 ESE
 *
 * License : CC BY
 *
 * Rev : 20231004
 *
 */

#include <stdio.h>
#include <math.h>

typedef struct {
		float Mod;
		float Arg;
	} Complexe_t ;


float x(Complexe_t *pz);
float y(Complexe_t *pz);

int main() {
	Complexe_t z1;
	z1.Mod = 1.0;
	z1.Arg = M_PI/4;

	// Appel des fonctions x et y pour calculer les parties réelle et imaginaire de z1
	float Re = x(&z1);
	float Im = y(&z1);

	//Affichage des resultats
	printf("x : %f ; y : %f\n", Re, Im);
}

/*
 * Fonction :	float x(Complexe_t *pz)
 *			Calcule la partie réelle d'un nombre complexe à partir de son module et de son argument.
 *
 * Paramètres :
 * - pz : Pointeur vers la structure Complexe_t contenant le nombre complexe.
 *
 * Retour : La partie réelle calculée.
 */
float x(Complexe_t *pz) {
	return (pz->Mod * cos(pz->Arg));
}

/*
 * Fonction :	float y(Complexe_t *pz)
 *			Calcule la partie imaginaire d'un nombre complexe à partir de son module et de son argument.
 *
 * Paramètres :
 * - pz : Pointeur vers la structure Complexe_t contenant le nombre complexe.
 *
 * Retour : La partie imaginaire calculée.
 */
float y(Complexe_t *pz) {
	return (pz->Mod * sin(pz->Arg));
}
