/*
 * Author : Killian LOUISE TP2 ESE
 *
 * License : CC BY
 *
 * Rev : 20231004
 *
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

typedef struct {
		float Mod;
		float Arg;
	} Complexe_t ;

int comparaison(const void *a, const void *b);

int main() {
    Complexe_t listeComplexes[] = {
        {3.0, 45.0},
        {1.0, 60.0},
        {2.0, 30.0} };

    printf("Avant le tri : ");


    int taille = sizeof(listeComplexes) / sizeof(listeComplexes[0]);

    for (int i = 0; i < taille; i++) {
        printf("Mod : %.2f, Arg : %.2f\n", listeComplexes[i].Mod, listeComplexes[i].Arg);
    }

    qsort(listeComplexes, taille, sizeof(Complexe_t), comparaison);

    // Maintenant, votre liste est triée en fonction de "Mod"

    // Affichez les éléments triés
    printf("Apres le tri : ");
    for (int i = 0; i < taille; i++) {
        printf("Mod : %.2f, Arg : %.2f\n", listeComplexes[i].Mod, listeComplexes[i].Arg);
    }

    return 0;
}

/*
 * Fonction : int comparaison(const void *a, const void *b)
 * 			Compare deux éléments de type Complexe_t en fonction de leur champ "Mod".
 *
 * Paramètres :
 * - a : Pointeur vers le premier élément de type Complexe_t à comparer.
 * - b : Pointeur vers le deuxième élément de type Complexe_t à comparer.
 *
 * Retour :
 * - Une valeur positive si le module de l'élément 1 est plus grand que celui de l'élément 2.
 * - 0 en cas d'égalité des modules.
 * - Une valeur négative si le module de l'élément 1 est inférieur à celui de l'élément 2.
 */
int comparaison(const void *a, const void *b) {
    Complexe_t *element1 = (Complexe_t *)a;
    Complexe_t *element2 = (Complexe_t *)b;

    /* Comparez les champs "Mod" des deux éléments en utilisant la soustraction
     * si le module de l'element 1 est plus grand que celui de l'element 2
     * alors retourne une valeur positive
     * si egalite -> 0
     * si module de 1 est inferieur au module de 2 -> negatif
     */
    return (element1->Mod - element2->Mod);
}
