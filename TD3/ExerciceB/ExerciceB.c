/*
 * Author : Killian LOUISE TP2 ESE
 *
 * License : CC BY
 *
 * Rev : 20231004
 *
 */

#include <stdio.h>
#include <stdlib.h>

float moyenne(float t[], int nb);

int main() {
	int N;
	printf("N ?\n");
	scanf("%i", &N);

	float *p = NULL;
	p = (float*)malloc(N*sizeof(float));
	if(p == (float*)NULL) {
		printf("Erreur !");
	}
	else {
		for(int i=0; i<N; i++) {
				printf("Saisir N%i : ", i);
				scanf("%f", (p+i));
				fflush(stdin);
				printf("Vous avez saisi %f\n", *(p+i));
			}
		printf("Moyenne : %f\n", moyenne(p, N));
	}

	free(p);
	p = NULL;
}

/*
 * Fonction :	float moyenne(float t[], int nb)
 *			Calcule la moyenne d'un tableau de nombres.
 *
 * Paramètres :
 * - t : Tableau de nombres.
 * - nb : Nombre d'éléments dans le tableau.
 *
 * Retour : La moyenne calculée.
 */
float moyenne(float t[], int nb) {
	float m = 0;
	for(int i=0; i <nb; i++) {
		m += t[i]/nb;
	}
	return m;
}

