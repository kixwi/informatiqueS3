/*
 * Author : Killian LOUISE
 * Copyright : CC BY
 * Rev : 20230920
 */

#include <stdio.h>

#define NBC		10

void afficheTab(int *t, int nb, int *pmax) {
	for(int i=0; i<nb; i++) {
		printf("i:%i;tab[i]:%i\n",i, t[i]);
	}
}

int main(void) {
	int tab[NBC] = {1,4,6,2,78,1,67,90,1,0};
	int max = 0;
	afficheTab(tab, NBC, max);

	return 0;
}
