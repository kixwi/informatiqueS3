/*
 * Author : Killian LOUISE TP2 ESE
 *
 * License : CC BY
 *
 * Rev : 20230924
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int compteurCaractere(char caractereRecherche, char *pphrase);

int main(void) {
	//Init pour essais
	char phrase[200] = "Ceci est une phrase test. Obi-Wan Kenobi est un Jedi";
	char caractereRecherche = 0;
	int caractereComptes = 0;

	//Affichage de la phrase analysee
	printf("Phrase analysee : %s\n", phrase);
	//Saisie de la lettre a compter
	printf("Quel caractere voulez-vous compter dans la phrase ?\n");
	do {
		scanf("%c", &caractereRecherche);
		//Si mauvaise saisie, recommence
		if((caractereRecherche<='A') && (caractereRecherche>='z')) {
			printf("Erreur de saisie, recommencez !");
		}
	}
	while((caractereRecherche<='A') && (caractereRecherche>='z'));

	//Appel de la fonction compteur
	caractereComptes = compteurCaractere(caractereRecherche, phrase);

	//Affichage du resultat
	printf("Nombre de %c dans la phrase : %i", caractereRecherche, caractereComptes);

	return 0;
}

/*	Fonction : int compteurCaractere(char caractereRecherche, char *pphrase)
 *			Compte le nombre d'occurence du caractere demande dans une chaine
 *			donnee
 *
 * 	Parametres :
 * 			char caractereRecherche : caractere a compter
 * 			char *pphrase : chaine de caractere phrase
 *
 * 	Retour :
 *			Nombre d'occurence de la lettre demandee
 */
int compteurCaractere(char caractereRecherche, char *pphrase) {
	//Init compteur
	int cpt = 0;

	/* strchr() retourne la un pointeur sur le prochain caractere demande.
	 * Incremenation de pphrase -> avance dans la chaine jusqu'a ce qu'il
	 * n'y ait plus de caractere demande.
	 * Incrementation de cpt -> nombre de caractere demande trouve
	 */
	while((pphrase=strchr(pphrase, caractereRecherche))!=0) {
		cpt = cpt + 1;
		pphrase = pphrase + 1;
	}

	//Retour nombre caractere trouve
	return cpt;
}
