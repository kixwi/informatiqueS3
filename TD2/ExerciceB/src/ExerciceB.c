/*
 * Author : Killian LOUISE TP2 ESE
 *
 * License : CC BY
 *
 * Rev : 20230924
 *
 */
#include <stdio.h>
#include <stdlib.h>

void copie_chaine(char *destination, char *source);
void compteur(int *resultat, char *source);

int main(void) {
	//Init de divers chaines pour essais
	char tab1[25] = "PHRASE DE COPIE";
	char tab2[50] = "Bonjour, ceci est une phrase banale";
	int result[26];
	char texte[100] = "ceci est une phrase test";

	//Affichage pour la copie
	printf("=== COPIE DE CHAINE ===\n");
	printf("Phrase copiee : %s\n", tab1);
	printf("Phrase destination : %s\n", tab2);
	printf("Copie en cours ...\n");

	//Appel de la copie
	copie_chaine(tab2, tab1);

	//Affichage resultats
	printf("Copie : %s\n", tab2);
	printf("Contenu de brut la chaine destination : ");
	for(int j=0; j<50; j++) {
		printf("%c",tab2[j]);
	}
	printf("\n");

	//Affichage comptage
	printf("=== COMPTAGE ===\n");
	printf("Phrase analysee : %s\n", texte);
	//Appel du compteur
	compteur(result, texte);

	return 0;
}

/*	Fonction : void copie_chaine(char *destination, char *source)
 *			Copie la chaine source dans la chaine destination
 *
 * 	Parametre :
 * 			chaines destination et source
 *
 * 	Retour :
 *			destination, avec source copiee
 */
void copie_chaine(char *destination, char *source) {
	/* Tant que la chaine source n'est pas finie,
	 * copie de source dans destination et incrementation
	 */
	while((*destination++=*source++)!='\0');
}

/*	Fonction : void compteur(int *resultat, char *source)
 *			Compte le nombre d'occurence des lettres minuscules
 *			dans le texte fourni en source
 *
 * 	Parametre :
 * 			chaines resultat et source
 *
 * 	Retour :
 *			int resultat, tableau d'entiers contenant le nombre
 *			d'occurence de chaque lettre
 */
void compteur(int *resultat, char *source) {
	//Mise a 0 du tableau d'occurences
	for(int i=0; i<26; i++) {
		*(resultat+i) = 0;

	}
	//Tant que pas fin de chaine
	while(*source!='\0') {
		//si caractere lu est une lettre minuscule -> comptage
		if((*source>='a') && (*source<='z')) {
			/* *source-'a' : determination de l'indice de la lettre lue
			 * (a->0;z->25)
			 * *(resultat+indice) <=> resultat[indice]
			 * Incrementation de l'occurence de la lettre lue
			 */
			*(resultat+(*source-'a')) = *(resultat+(*source-'a')) + 1;
		}

		//Caractere suivant
		source++;
	}
	//Affichage du resultat, sous forme "n de a : X"
	for(int j=0; j<26; j++) {
		printf("n de %c : %i\n", j+'a', *(resultat+j));
	}
}
