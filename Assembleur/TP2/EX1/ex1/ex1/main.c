/*
 * ex1.c
 *
 * Created: 06/10/2023 11:16:36
 * Author : User
 */ 

#include <avr/io.h>

extern void tempo();

int main(void)
{
    /* Replace with your application code */
	DDRB |= (1<<PORTB5);
    while (1) 
    {
		PORTB |= (1<<PORTB5);
		tempo();
		PORTB &= ~(1<<PORTB5);
		tempo();
	}
}

