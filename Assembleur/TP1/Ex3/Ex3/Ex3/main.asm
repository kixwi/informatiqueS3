;-------------------------------
; IUT Limousin
; Dept GEII Brive
; Info embarqu�e S3 - micro
; OH 09/2022
; Point de d�part TP assembleur
;-------------------------------

.include "m328Pdef.inc"		; D�finition des registres de l'atmega28P

.cseg						; Debut du code segment

.equ	delay = 15625

.org	0					; Adresse 0 : vecteur d'Interruption RESET
		rjmp	Debut

.org	0x0100				
; Debut programme, d�cal� � l'adresse $100 
; pour ne pas �craser les vecteurs d'IT
Debut:						
;Init du pointeur de pile SP. RAMEND = @ du dernier octet dans la RAM
		ldi		r16,high(RAMEND)
		out		SPH,r16
		ldi		r16,low(RAMEND)	
	   	out		SPL,r16

;Init de la broche LED L en sortie : A COMPLETER
		;in		r16, DDRB
		ldi		r16, 1<<DDB5
		out		DDRB, r16
		
;Boucle sans fin 
loop:
		; Allumer LED L : A COMPLETER
		;in r16, 
		ldi		r16, 1<<DDB5
		out		PORTB, r16	

		CLR		R17
		call	pause_10s

		; �treindre LED L : A COMPLETER
		clr		r16
		out		PORTB, r16

		CLR		R17
		call	pause_10s

		rjmp	loop
;Fin programme

;routine de temporisation de d�part : 
;environ 16 ms avec une fr�quence d'horloge de 16MHz
;boucle d'attente sur 16bits							
tempo:
		ser	r30			; Z <- $FFFF
		ser	r31			; .
		
wait:							
		IN		R16, TIFR1		;copie de tifr1 dans r16
		SBRS	R16, OCF1A		;test si ocf1a = 1
		RJMP	wait			;reboucle
		ret						;retour

pause_1s :
		clr		r16				
		sts		TCNT1H, r16
		sts		TCNT1L, r16
		ldi		r16, high(delay)
		STS		OCR1AH, R16
		ldi		r16, low(delay)
		sts		OCR1AL, r16
		clr		r16
		sts		TCCR1A, r16
		ldi		r16, (1<<CS12)|(1<<CS10)
		sts		TCCR1B, R16
		CLR		R16
		STS		TCCR1C, R16
		CALL	WAIT
		LDI		R16, 1<<OCF1A
		OUT		TIFR1, R16
		CLR		r16
		STS		TCCR1B, R16
		RET

;EX4
pause_10s :
		; RAZ R17
		CLR		R17

BOUCLE_PAUSE_10S :
		SUBI	R17, -1			; Increment de r17
		CALL	PAUSE_1S		; Appel pause_1s
		CPI		R17, 10			; Comparaison r17, 10 (r17<10)
		BRNE	BOUCLE_PAUSE_10S; Pas egal -> reboucle
		RET						; SI R17 = 10, RETOUR
