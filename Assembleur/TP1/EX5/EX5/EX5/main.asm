;-------------------------------
; IUT Limousin
; Dept GEII Brive
; Info embarqu�e S3 - micro
; OH 09/2022
; Point de d�part TP assembleur
;-------------------------------

.include "m328Pdef.inc"		; D�finition des registres de l'atmega28P

.cseg						; Debut du code segment

.equ	delay = 15625

.org	0					; Adresse 0 : vecteur d'Interruption RESET
		rjmp	Debut

.ORG	OC1Aaddr
		RJMP TIMER1

.org	0x0100				
; Debut programme, d�cal� � l'adresse $100 
; pour ne pas �craser les vecteurs d'IT
Debut:						
;Init du pointeur de pile SP. RAMEND = @ du dernier octet dans la RAM
		ldi		r16,high(RAMEND)
		out		SPH,r16
		ldi		r16,low(RAMEND)	
	   	out		SPL,r16

;Init de la broche LED L en sortie : 
		;in		r16, DDRB
		ldi		r16, 1<<DDB5
		out		DDRB, r16

		SEI

		CALL	CONFIG_TIMER
		
;Boucle sans fin 
loop:
		rjmp	loop

CONFIG_TIMER :
		
		clr		r16					;INIT TCNT1
		sts		TCNT1H, r16
		sts		TCNT1L, r16
		ldi		r16, high(delay)	;OCR1A<-15625
		STS		OCR1AH, R16
		ldi		r16, low(delay)
		sts		OCR1AL, r16
		clr		r16					
		sts		TCCR1A, r16			;TCCR1A<-0
		ldi		r16, (1<<CS12)|(1<<CS10)|(1<<WGM12)
		sts		TCCR1B, R16			;TCCR1B<-(1<<CS10)|(1<<CS12)|(1<<WGM12)
		CLR		R16					;TCCR2B<-0
		STS		TCCR1C, R16			
		LDI		R16, 1<<OCIE1A		;PROGRAMMATION INTERRUPTION
		STS		TIMSK1, R16			
		RET

/* CA FONCTIONNE
;ROUTINE IT TIMER1
TIMER1 :
		;IN		R18, PORTB
		SBIS	PINB, 5
		RJMP	CAS_0
		CBI		PORTB, 5
		RJMP	FIN_IT

CAS_0 :
		SBI		PORTB, 5

FIN_IT :
		RETI
*/

TIMER1 :
		IN		R17, PORTB
		LDI		R18, 1<<PORTB5
		EOR		R17, R18
		OUT		PORTB, R17
		RETI