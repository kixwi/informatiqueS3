;-------------------------------
; IUT Limousin
; Dept GEII Brive
; Info embarqu�e S3 - micro
; OH 09/2022
; Point de d�part TP assembleur
;-------------------------------

.include "m328Pdef.inc"		; D�finition des registres de l'atmega28P

.cseg						; Debut du code segment
.org	0					; Adresse 0 : vecteur d'Interruption RESET
		rjmp	Debut

.org	0x0100				
; Debut programme, d�cal� � l'adresse $100 
; pour ne pas �craser les vecteurs d'IT
Debut:						
;Init du pointeur de pile SP. RAMEND = @ du dernier octet dans la RAM
		ldi		r16,high(RAMEND)
		out		SPH,r16
		ldi		r16,low(RAMEND)	
	   	out		SPL,r16

;Init de la broche LED L en sortie : A COMPLETER
		;in		r16, DDRB
		ldi		r16, 1<<DDB5
		out		DDRB, r16
		
;Boucle sans fin 
loop:
		; Allumer LED L : A COMPLETER
		;in r16, 
		ldi		r16, 1<<DDB5
		out		PORTB, r16	

		call	tempo
		call	tempo
		call	tempo
		call	tempo
		call	tempo

		; �treindre LED L : A COMPLETER
		clr		r16
		out		PORTB, r16

		call	tempo
		call	tempo
		call	tempo
		call	tempo
		call	tempo

		rjmp	loop
;Fin programme

;routine de temporisation de d�part : 
;environ 16 ms avec une fr�quence d'horloge de 16MHz
;boucle d'attente sur 16bits							
tempo:
		ser	r30			; Z <- $FFFF
		ser	r31			; 
wait:							
		sbiw	r30,1			; d�cr�mente Z de 1
		brne	wait			; boucle si Z non nul
		ret
