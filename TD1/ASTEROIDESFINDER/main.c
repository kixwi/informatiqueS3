/*
 * Author : Killian LOUISE TP2 ESE
 *
 * License : CC BY 4.0
 *
 * Rev : 20230923.5
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#include "champasteroides.h"

//#define DEBUG
#define VERSION 20230923.5

#define delay 	1500

int main()
{
    int nDetectedAsteroids = 0;
    int choixMode = 0;

	#ifdef DEBUG
    printf("DEBUG MODE\nVERSION : %.1f\n\n", VERSION);
	#endif

    //Selection du mode
    choix(&choixMode);

	#ifdef DEBUG
	printf("MAIN : Choix mode : %i\n\r", choixMode);
	#endif

	//Si mode 2, -> affichage d'un texte de contexte
	if(choixMode==2) {
		HANDLE H = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleTextAttribute(H,0+3);

		printf("\n\n");
		printf("IL Y A BIEN LONGTEMPS, DANS UNE GALAXIE \nLOINTAINE, TRES LOINTAINE....\n");
		Sleep(2*delay);
		printf("\n\n");

		SetConsoleTextAttribute(H,0+14);

		printf(	"APRES AVOIR COMBATTU L'EMPIRE SUR LA PLANETE HOTH,\n");
		Sleep(delay);
		printf(	"LE REBELLE HAN SOLO, ACCOMPAGNE DE LA PRINCESSE\n");
		Sleep(delay);
		printf(	"LEIA ORGANA ET DE SES FIDELES DROIDES SE RETROUVENT\n");
		Sleep(delay);
		printf(	"POURSUIVIES PAR LES FORCES IMPERIALES.\n");
		Sleep(delay);
		printf(	"LE CAPITAINE DE FAUCON MILLENIUM DECIDE D'ENTRER DANS\n");
		Sleep(delay);
		printf(	"UN CHAMP D'ASTEROIDES POUR SEMER LES FORCES DU MAL...\n\n");
		Sleep(2*delay);

		SetConsoleTextAttribute(H,10);

		printf(	"LE PIRATE HAN SOLO VOUS A ENGAGE A BORD DU FAUCON\n");
		Sleep(delay);
		printf("MILLENIUM GRACE A VOS COMPETENCES EN PROGRAMMATION\n");
		Sleep(delay);
		printf("D'ORDINATEUR DE BORD. C'EST LE MOMENT DE JUSTIFIER\n");
		Sleep(delay);
		printf("VOTRE PRIX : PROGRAMMEZ L'ORDINATEUR DE BORD POUR\n");
		Sleep(delay);
		printf("EVITER DE RENTRER EN COLISION AVEC UN ASTEROIDE !\n\n");

		SetConsoleTextAttribute(H,15);
	}

	//Selection de l'image/champ d'asteroides
	selectionImage();

	//Affichage du champ selectionne
    affichage(image);

    //Comptage des asteroides uniques
    compteurAsteroides(&nDetectedAsteroids);

    //Affichage du champ marque
    affichage(image);

    //Retour du nombre d'asteroides comptabilises
    printf("L'ordinateur de bord a detecte %i asteroides.\n", nDetectedAsteroids);

    //Si mode 2 -> affichage texte contexte de fin
	if(choixMode==2) {
		HANDLE H = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleTextAttribute(H,0+14);
		printf(	"GRACE A VOS TALENTS DE PROGRAMMATION, LES REBELLES\n");
		Sleep(delay);
		printf(	"ONT PU FUIR LES TERRIBLES FORCES DU MAL !\n");
		Sleep(delay);
		printf(	"VOUS ETES UN REBELLE DANS L'AME, BRAVO !\n");
		SetConsoleTextAttribute(H,15);
	}

	return 0;
}

/*	Fonction : void affichage(image_t image)
 *			Affiche le tableau (image du champ d'asteroides)
 *
 * 	Parametre :
 * 			type image_t image : tableau contenant le champ d'asteroides
 *
 * 	Retour :
 *			aucun
 */
void affichage(image_t image) {
	//affichage du champ d'asteroides
    for(int i=0; i<NBL; i++) {
        for(int j=0; j<NBC; j++) {
            printf("%c", image[i][j]);
            printf("  ");
        }
        printf("\n\r");
    }
}

/*	Fonction : int i_j_to_num(int i, int j)
 *			Convertir des coordonnees en numero de case
 *
 * 	Parametres :
 * 			int i : numero de la ligne
 * 			int j : numero de la colonne
 *
 * 	Retour :
 * 			numero de la case
 */
int i_j_to_num(int i, int j) {
    return i*NBC+j;
}

/*	Fonction : int num_to_i(int num)
 *			Recupere le numero de ligne d'un numero de case
 *
 * 	Parametre :
 * 			int num : numero de la case
 *
 * 	Retour :
 * 			numero de ligne
 */
int num_to_i(int num) {
    return num/NBL;
}

/*	Fonction : int num_to_j(int num)
 *			Recupere le numero de colonne d'un numero de case
 *
 * 	Parametre :
 * 			int num : numero de la case
 *
 * 	Retour :
 * 			numero de colonne
 */
int num_to_j(int num) {
    return num%NBC;
}

/*	Fonction : int recherche_suivant(image_t image, int num)
 *			Recherche le prochain asteroide/morceau d'asteroides
 *
 * 	Parametre :
 * 			image_t image, int num
 *
 * 	Retour :
 * 			num de la case asteroide trouve
 */
int recherche_suivant(image_t image, int num) {
    int finTableau = 0;
    //Conversion num -> coord x;y
    int x = num_to_i(num);
    int y = num_to_j(num);
    //Tant que la case n'est pas une case asteroide, continue la recherche
    while(image[x][y]!='X' && finTableau==0) {
        y = y + 1;
        //Fin ligne -> ligne suivqnte
        if(y==NBC) {
            y = 0;
            x = x + 1;
        }
        //Fin tableau -> finTableau = !
        if(x>=NBL) {
				#ifdef DEBUG
                printf("Fin tableau\n");
				#endif
                finTableau = 1;
        }
    }
    //Si finTableau -> num negatif
    if(finTableau==1) {
        x = -1;
        y = -1;
    }
	#ifdef DEBUG
    //printf("Asteroide aux coord. : x:%i;y:%i\n", x, y);
	#endif
    //Retour du numero de la case asteroide
    return i_j_to_num(x, y);
}

/*	Fonction : int marquage(image_t image, int num)
 *			Marque les asteroides
 *
 * 	Parametre :
 * 			image_t image, int num
 *
 * 	Retour :
 * 			OK si asteroides marques, NOK si non
 */
int marquage(image_t image, int num) {
	int R;
	//Conversion du num en coord. tableau (i et j)
	int posI = num_to_i(num);
	int posY = num_to_j(num);
	//si la case demandee n'est pas un asteroide, fin fonction
	if((posI>NBL || posI<0) || (posY>NBC || posY<0) || (image[posI][posY]!='X')) {
		R = NOK;
	}
	//Si la case est bien un asteroide, marquage et detection alentour
	else {
		//Marquage de la case asteroide
		image[posI][posY] = 'O';
		/*	Appel de la fonction marquage sur les huit cases alentours
		 * 	Schema :
		 * 			X X X
		 * 			X O X
		 * 			X X X
		 * 		X -> cases où la présence d'astéroide est testée
		 * 		0 -> case marquée
		 */
		marquage(image, i_j_to_num(posI, 	posY+1));
		marquage(image, i_j_to_num(posI+1, 	posY+1));
		marquage(image, i_j_to_num(posI+1, 	posY));
		marquage(image, i_j_to_num(posI+1, 	posY-1));
		marquage(image, i_j_to_num(posI, 	posY-1));
		marquage(image, i_j_to_num(posI-1, 	posY-1));
		marquage(image, i_j_to_num(posI-1, 	posY));
		marquage(image, i_j_to_num(posI-1, 	posY+1));

		R = OK;
	}
	return R;
}

/*	Fonction : void compteurAsteroides(int *pNDetectedAsteroids)
 *			Compte le nombre d'asteroides distincts sur une image
 *
 * 	Parametre :
 * 			pointeur pNDetectedAsteroids(retour de valeur par parametre,
 * 			pour l'exercice (non-impose)) : nombre d'asteroides trouves
 *
 * 	Retour :
 * 			nombre d'asteroides trouves
 */
void compteurAsteroides(int *pNDetectedAsteroids) {
	//int numScan : numero de la case analysee
	int numScan = 0;

	//Faire tant que numScan est "dans" le tableau (>= a 0)
	do {
		// avancement au prochain asteroide non marque
	    numScan = recherche_suivant(image, numScan);
		#ifdef DEBUG
	    printf("NumScan : %i\n\r", numScan);
		#endif

	    /* si numScan >= 0 -> marquage de l'asteroide et incrementation
	     * compteur d'asteroides detectes (numScan negatif si en dehors
	     * du tableau)
	     */
	    if((numScan>=0)) {
	    	//marquage asteroide
	    	marquage(image, numScan);
	    	//incrementation compteur
	    	*pNDetectedAsteroids = (*pNDetectedAsteroids) + 1;
			#ifdef DEBUG
	    	printf("NDA : %i\n\r", *pNDetectedAsteroids);
	    	affichage(image);
	    	printf("\n\n");
			#endif
	    }

	    //avancement de la case analysee
	    numScan = numScan + 1;
	}
	while(numScan>=0);

	printf("\n\n");
}

/*	Fonction : void choix(int *pchoixMode)
 *			Recupere un choix de l'utilisateur
 *
 * 	Parametre :
 * 			pointeur pchoixMode (retour de valeur par parametre,
 * 			pour l'exercice (non-impose))
 *
 * 	Retour :
 * 			valeur du choix, par parametre
 */
void choix(int *pchoixMode) {
	int tmp = 0;
	//Affichage des choix
	printf(	"Dans quel mode souhaitez-vous participer ?\n"
			"    1-Mode normal\n"
			"    2-Mode histoire\n");
	//Recuperation de la saisie utilisateur
	scanf(" %i", &tmp);
	//Si saisie incorrecte -> redemande une saisie
	while((tmp!=1)&&(tmp!=2)) {
		printf("ERREUR, RECOMMENCEZ !\n");
		scanf(" %i", &tmp);
	}

	//Retour de la valeur saisie
	*pchoixMode = tmp;
	#ifdef DEBUG
	printf("Choix mode : %i\n\r", *pchoixMode);
	#endif
}

/*	Fonction : void selectionImage()
 *			Recupere un choix de l'utilisateur et met l'image voulue dans
 * 			le tableau image
 *
 * 	Parametre :
 * 			aucun
 *
 * 	Retour :
 * 			aucun
 */
void selectionImage() {
	int tmp = 0;
	//Affichage des choix
	printf(	"Quel champ d'asteroides voulez-vous parcourir ?\n"
			"    1\n"
			"    2\n");
	//Recuperation de la saisie utilisateur
	scanf(" %i", &tmp);
	//Si saisie incorrecte -> redemande une saisie
	while((tmp!=1)&&(tmp!=2)) {
		printf("ERREUR, RECOMMENCEZ !\n");
		scanf(" %i", &tmp);
	}

	/* En fonction du choix utilisateur, on recopie l'image demandee
	 * dans le tableau image
	 */
	switch (tmp) {
	case 1 :
		//Recopie
	    for (int i = 0; i < NBL; i++) {
	        for (int j = 0; j < NBC; j++) {
	            image[i][j] = image1[i][j];
	        }
	    }
		break;
	case 2 :
		//Recopie
	    for (int i = 0; i < NBL; i++) {
	        for (int j = 0; j < NBC; j++) {
	            image[i][j] = image2[i][j];
	        }
	    }
		break;
	}

	#ifdef DEBUG
	printf("Choix image : %i\n\r", tmp);
	#endif
}
