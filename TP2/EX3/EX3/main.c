// OH pour TP micro - Arduino UNO - IUT GEII 2023
// TP 2 C sur AVR, point de d�part


/* Includes */
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>

/* Prototypes */
void USART_Init( unsigned int baudrate );
unsigned char USART_Receive( void );
void USART_Transmit( unsigned char data );
void pause() ;
void system_init() ;

static FILE mystdout=FDEV_SETUP_STREAM(USART_Transmit, NULL, _FDEV_SETUP_WRITE);

/*
 * initialisations 
 */
void system_init()
{
	// Init port B pour LED L
	DDRB |= 1 << PORTB5 ;	// PORTB5 en sortie. LED L �teinte.
	PORTB &= ~(1 << PORTB5) ;
	
	// Init USART, 19200
	USART_Init( 51 ); 
}

/*
 * Fonctions USART
 * ---------------
 */
/* Initialisation  */
void USART_Init( unsigned int baudrate )
{
	/* Baud rate */
	UBRR0H = (unsigned char) (baudrate>>8);
	UBRR0L = (unsigned char) baudrate;
	
	/* RAZ du registre de controle A	*/
	UCSR0A = 0x00 ;

	/* Validation USART receiver et transmitter */
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) ;
	
	/* Configuration : 8 data, 2 stop, pas de parit� */
	UCSR0C = (1<<USBS0) | (1<<UCSZ01) | (1<<UCSZ00);
	
	/*Ajout interruption RX*/
	/*NE PAS OUBLIER LE |= !!!*/
	UCSR0B |= (1<<RXCIE0);
}


/* Read and write functions */
unsigned char USART_Receive( void )
{
	/* Wait for incomming data */
	while ( !(UCSR0A & (1<<RXC0)) )
	;
	
	/* Return the data */
	return UDR0;
}

void USART_Transmit( unsigned char data)
{
	/* Wait for empty transmit buffer */
	while ( !(UCSR0A & (1<<UDRE0)) )
	;
	/* Start transmittion */
	UDR0 = data;
}


/*
 * Fonctions Timer1
 * ----------------
 */
/* Fonction pause : 1 seconde	*/
void pause()
{
	/*
	 * Initialisation du timer. On utilise le mode normal (Modifier pour interruption)
	 */
	TCNT1=0;		// init compteur
	OCR1A=15625 ;	// Valeur de comparaison pour 1s
	TCCR1A=0; 		// mode normal (overflow), 
	TCCR1B=0x05; 	// prescaler /1024
	TCCR1C=0; 		

	while((TIFR1 & (1 << OCF1A)) == 0) ;	// attente comparaison

	TIFR1 |= (1 << OCF1A) ;					// RAZ flag OCF1A
	TCCR1B=0; 								// Arr�t timer
}

/*
 * Programme principal
 */
int main(void)
{
	stdout = &mystdout;
	system_init();	
	sei();


	
	
	
	while(1)
	{
		PORTB |= (1<<PORTB5);
		pause();
		PORTB &= ~(1<<PORTB5);
		pause();
		PORTB |= (1<<PORTB5);
		pause();
		PORTB &= ~(1<<PORTB5);
		pause();
		SMCR = 0x0;
		PRR = (1<<PRTWI)|(1<<PRTIM2)|(1<<PRTIM0)|(1<<PRSPI)|(1<<PRADC);
		SMCR |= (1<<SE);
		asm volatile("sleep" "\n\t");
	}
}

ISR(USART_RX_vect) {
	unsigned char c1 = UDR0;
	//printf("Caractere recu : %c\n\r", c1);
	USART_Transmit(c1);
}

/*
ISR(TIMER1_COMPA_vect) {
	PORTB ^= (1<<PORTB5);
}
*/