/*
 * Author : Killian LOUISE TP2 ESE
 *
 * License : CC BY
 *
 * Rev : 20231009
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include "tableaudyn.h"

void afficherTableauDyn(tableauDyn *pt) {
	//Affichage capacite
	printf("-------------------------\n");
	printf("Capacite : %i\n", pt->capacite);
	//Affichage Nombre
	printf("-------------------------\n");
	printf("Nombre : %i\n", pt->nombre);
	printf("-------------------------\n");
	//Affichage des elements du tableau
	for(int i=0; i<pt->nombre; i++) {
		printf("Tab[%i] : %.1f\n", i, pt->tab[i]); //aussi pt->tab[i]
	}
	printf("-------------------------\n");

}

tableauDyn *creerTableauDyn(int capa){
	tableauDyn *pt;
	float *t;
	t = (float *)malloc(capa*sizeof(float));
	if(t==(float *)NULL) {
		#ifdef DEBUG
		printf("tableauDyn.c : Erreur creation tab dyn %d\n", __LINE);
		#endif
		pt = (tableauDyn *)NULL;

	}
	else {
		pt = (tableauDyn *)malloc(sizeof(tableauDyn));
		if(pt==(tableauDyn *)NULL) {
			#ifdef DEBUG
			printf("tableauDyn.c : Erreur creation pt %d\n",__LINE);
			#endif
		}
		else {
			pt->capacite = capa;
			pt->nombre = 0;
			pt->tab = t;
		}
	}
	return pt;
}

int ajouterValeur(tableauDyn *pt, float v){
	if((pt->nombre)>(pt->capacite)) {
		printf("tableauDyn.c Erreur capacite\n");
		redimensionner(pt);
		ajouterValeur(pt, v);
	}
	else {
		pt->tab[pt->nombre] = v;
		pt->nombre = pt->nombre + 1;
	}

	return 1;
}

int redimensionner(tableauDyn *pt) {
	float *tas = (float *)realloc(pt->tab, (pt->capacite+10)*sizeof(float));
	if(tas==(float *)NULL) {
		//printf("tableauDyn.c Erreur redi %d\n",__LINE);
		return -1;
	}
	else {
		pt->tab = tas;
		pt->capacite = pt->capacite+10;
		return 1;
	}
}

int nombreValeurs(tableauDyn *pt) {
	return pt->nombre;
}

float valeur(tableauDyn *pt, int i) {
	return pt->tab[i-1];
}

void supprimerTableauDyn(tableauDyn *pt) {
	free(pt);
}
