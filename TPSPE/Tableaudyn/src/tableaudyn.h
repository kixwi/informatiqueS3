/*
 * tableaudyn.h
 *
 *  Created on: 9 Oct 2023
 *      Author: User
 */

#ifndef TABLEAUDYN_H_
#define TABLEAUDYN_H_

typedef struct {
	int capacite;
	int nombre;
	float *tab;
}tableauDyn;

tableauDyn *creerTableauDyn(int capa);
int ajouterValeur(tableauDyn *pt, float v);
int nombreValeurs(tableauDyn *pt);
float valeur(tableauDyn *pt, int i);
void supprimerTableauDyn(tableauDyn *pt);
int redimensionner(tableauDyn *pt);
void afficherTableauDyn(tableauDyn *pt);

#endif /* TABLEAUDYN_H_ */
