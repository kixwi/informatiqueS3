/*
 * Author : Killian LOUISE TP2 ESE
 *
 * License : CC BY
 *
 * Rev : 20231009
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include "tableaudyn.h"

#define DEBUG

int main() {

	tableauDyn *pt;
	pt = creerTableauDyn(4);
	if (pt==(tableauDyn *)NULL) {
		#ifdef DEBUG
		printf("Erreur creation\n");
		#endif
		exit(1);
	}

	afficherTableauDyn(pt);
	ajouterValeur(pt, 5.0);
	afficherTableauDyn(pt);
	ajouterValeur(pt, 15.0);

	#ifdef DEBUG
	afficherTableauDyn(pt);
	#endif

	printf("2e valeur : %.1f", valeur(pt, 2));

	ajouterValeur(pt, 58.0);
	ajouterValeur(pt, 578.5);
	ajouterValeur(pt, 5.50);

	afficherTableauDyn(pt);




	return 0;
}
